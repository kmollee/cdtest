﻿from bs4 import BeautifulSoup
 
def _remove_attrs(soup):
    for tag in soup.findAll(True): 
        tag.attrs = None
    return soup
 
 
def example():
    filepath = 'V:/2014cd/cdwp/pandoc/demo.html'
    doc = ''
    with open(filepath, 'tr', encoding='utf-8') as infile:
        doc = infile.read()
    #print(doc)
    doc = doc.encode('utf-8')
    print(doc)
    soup = BeautifulSoup(doc)
    body_tag = str(soup.body.extract())
    #將其 tag 轉為 bs4 類別
    body_tag = BeautifulSoup(body_tag)
    #將 body_tag body tag  脫離
    body_tag.body.unwrap()
 
    clean_soup = _remove_attrs(body_tag)
    print('\nAfter:\n%s' % clean_soup)

    with open('test.html', 'w', encoding='utf-8') as outfile:
        outfile.write(str(clean_soup))

    '''
    for line in infile:
        print(line)
    '''
    #doc = '<html><head><title>test</title></head><body id="foo" onload="whatever"><p class="whatever">junk</p><div style="background: yellow;" id="foo" class="blah">blah</div></body></html>'
    #print('Before:\n%s' % doc)
    '''
    soup = BeautifulSoup(doc)
    clean_soup = _remove_attrs(soup)
    print('\nAfter:\n%s' % clean_soup)
    '''
example()